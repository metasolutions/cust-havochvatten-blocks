window.__entryscape_config = {
  block: 'config',
  page_language: 'sv',
  collections: [
    {
      type: 'facet',
      name: 'terminology',
      label: 'Terminologier',
      property: 'skos:inScheme',
      nodetype: 'uri',
      limit: 10,
    }],
  blocks: [
    {
      block: 'terminologyButton',
      extends: 'link',
      relation: 'skos:inScheme',
      namedclick: 'terminology',
      content: '${label}'
    },
    {
      block: 'broaderButton',
      extends: 'link',
      relation: 'skos:broader',
      click: './',
      content: 'Överliggande begrepp: ${skos:prefLabel}'
    },
    {
      block: 'conceptSearch',
      extends: 'searchList',
      rdftype: 'skos:Concept',
      rdformsid: 'skosmos:concept',
      initsearch: true,
	facets: true,
	namedclick: 'concept',
      headless: true,
      rowhead: '{{text namedclick="terminology" relation="skos:inScheme" }}' +
        '<h2>{{text namedclick="concept"}}</h2>' +
        '<div class="esbDescription">{{ text content="${skos:definition}" }}</div>',
	rowexpand: '{{view}}',
      limit: 8
    },
    {
      block: 'conceptSearchInTemplate',
      extends: 'list',
      relation: 'skos:hasTopConcept',
      define: 'conceptSearchInTemplate',
      placeholder: "Sök efter begrepp ...",
      headless: false,
      rowhead: '{{link btn btn-sm btn-default primaryBtn" content="Gå till begrepp" namedclick="concept"}}' +
        '<h2>{{text namedclick="concept"}}</h2>' +
        '<div class="esbDescription">{{ text content="${skos:definition}" }}</div>',
      rdftype: 'skos:Concept',
      rdformsid: 'skosmos:concept',
      initsearch: true,
      limit: 8
    },
    {
      block: 'toppbegrepp',
      extends: 'template',
      template: '{{#ifprop "rdf:type" uri="skos:ConceptScheme"}}<h2>Toppbegrepp</h2>{{toppbegreppLista}}{{/ifprop}}'
    },
    {
      block: 'toppbegreppLista',
      extends: 'list',
      listplaceholder: 'Det finns inga relaterade begrepp',
      namedclick: 'test',
      relation: "skos:hasTopConcept",
      layout: 'raw',
      limit: 5,
      rowhead: '{{link namedclick="concept"}}'
    },
    {
      block: 'hemvist',
      loadEntry: true,
      run: function (node, data, items, entry) {
        var resourceURI = entry.getResourceURI();
          node.innerHTML = "Adress för begreppet: <a href=\"" + resourceURI + "\">" + resourceURI + "</a>";
      }
    },
    {
      block: 'conceptResults',
      extends: 'results',
      use: 'conceptSearchInTemplate',
      templateFilter: '{{resultsize}} matchande begrepp i terminologin',
      templateFilterNoResults: 'Din sökning matchar inga begrepp i denna terminologi',
      templateNoFilter: '{{resultsize}} begrepp i terminologin',
    },
    {
      block: 'indexLink',
      extends: 'template',
      htemplate: '<a class="btn btn-default primaryBtn" href="/begrepp.html">' +
        'Tillbaka till sök' +
        '</a>',
    },
    {
      block: 'conceptSearchHav',
      extends: 'template',
      template: '<div class="conceptSearch">'
              + '  {{multiSearch placeholder="Sök efter begrepp..." open-on-focus="false"}}'
              + '  {{conceptSearch}}'
    },
    { 
      block: 'conceptDetailsHav',
      extends: 'template',
      template: '<div class="conceptDetailsHav"><h1>{{text content="${skos:prefLabel}" define="concept"}}</h1>'
      + '<div class="rdfLinks">'
      + '  Ladda ner begreppet:'
      + '  <a href="{{ metadataURI}}"'
      + '    target="_blank">RDF/XML</a>,'
      + '  <a href="{{ metadataURI }}?format=text/turtle"'
      + '    target="_blank">TURTLE</a>,'
      + '  <a href="{{ metadataURI }}?format=text/n-triples"'
      + '    target="_blank">N-TRIPLES</a>,'
      + '  <a href="{{ metadataURI }}?format=application/ld+json"'
      + '    target="_blank">JSON-LD</a>'
      + '</div>'
      + '{{hemvist}}'
      + '<p class="description">{{text content="${skos:definition}" use="concept" fallback="Ingen definition given för begreppet"}}</p>'
      + 'Terminologi: {{terminologyButton}}'
      + '{{#ifprop "skos:broader"}}, {{broaderButton}}{{/ifprop}}'
      + '<h2>Underordnade begrepp</h2>'
      + '{{list relation="skos:narrower" use="concept" namedclick="concept" listplaceholder="Det finns inga underordnade begrepp"}}'
      + '<h2>Relaterade begrepp</h2>'
      + '{{list relation="skos:related" use="concept" namedclick="concept" listplaceholder="Det finns inga relaterade begrepp"}}'
      + '<h2>Detaljer om begreppet</h2>'
      + '{{view rdformsid="skosmos:concept" filterpredicates="skos:narrower,skos:broader,skos:inScheme,skos:topConceptOf"}}</div>'
      + '<div class="hierarchy">{{hierarchy scale="2"}}</div>'
    },
    {
      block: 'conceptFacetsHav',
      extends: 'facets'
    },
    { 
      block: 'terminologyDetailsHav',
      extends: 'template',
      template: '<h1>{{text content="${label}" define="terminology"}}</h1>'
      + '<div class="rdfLinks">'
      + '  Ladda ner terminologin:'
      + '  <a href="{{ metadataURI}}"'
      + '    target="_blank">RDF/XML</a>,'
      + '  <a href="{{ metadataURI }}?format=text/turtle"'
      + '    target="_blank">TURTLE</a>,'
      + '  <a href="{{ metadataURI }}?format=text/n-triples"'
      + '    target="_blank">N-TRIPLES</a>,'
      + '  <a href="{{ metadataURI }}?format=application/ld+json"'
      + '    target="_blank">JSON-LD</a>'
      + '</div>'
      + '<p class="description">{{text content="${dcterms:description}" use="terminology" fallback="Ingen beskrigning given för terminologin"}}</p>'
      + '{{toppbegrepp}}'
      + '<div class="hierarchy">{{hierarchy scale="2"}}</div>'
    }
  ],
  type2template: {
    'skos:Concept': 'skosmos:concept',
  },
};
