window.__entryscape_config = (window.__entryscape_config || []).concat([{
  "!bundles": [
    "https://metadata.havochvatten.se/blocks/dcat-ap_se2.json",
    "https://metadata.havochvatten.se/theme/templates/nmdp4props.json",
    "https://metadata.havochvatten.se/theme/templates/nmdp4profiles.json",
    "https://metadata.havochvatten.se/theme/templates/information.json"
  ],
  type2template: {
    'dcat:Catalog': 'dcat:Catalog',
    'dcat:Dataset': 'hav:DatasetGeo',
    'dcat:Distribution': 'hav:DistributionGeo',
    'dcat:DataService': 'hav:DataServiceGeo',
    'vcard:Individual': 'dcat:contactPoint',
    'vcard:Organization': 'dcat:contactPoint',
    'vcard:Kind': 'dcat:contactPoint',
    'foaf:Agent': 'dcat:foaf:Agent',
    'foaf:Person': 'dcat:foaf:Agent',
    'foaf:Organization': 'dcat:foaf:Agent',
    'foaf:Document': 'dcat:Documentish',
    'dcterms:LicenseDocument': 'dcat:Documentish',
    'dcterms:Standard': 'dcat:Documentish',
  },
  collections: [
    {
      type: 'facet',
      name: 'theme',
      label: 'Tema (GEMET)',
      property: 'dcterms:subject',
      nodetype: 'uri',
      rdftype: 'skos:Concept',
      limit: 7,
    },
    {
	name: 'org',
//	label: 'Organisation',
	includeAsFacet: false
    }
  ],
  baseMap: {
    type: 'wms',
    title: 'Enkel grundkarta från HaV',
    attributions: 'Havs- och vattenmyndigheten, HaV</br>',
    name: 'hav-grundkarta',
    base: true,
    url: 'https://geodata.havochvatten.se/geoservices/hav-bakgrundskartor/ows?',
  },
  linkBehaviour: 'dialogWithResourceLink',
  urlQueryParameters: true,
  blocks: [{
    block: 'datasetSearchHav',
    extends: 'template',
    template: '{{multiSearch placeholder="Sök datamängder"}}\n'
            + '{{datasetList pageInUrl=true}}'
    },
    {
      block: 'datasetDetailsHav',
      extends: 'template',
      template: '<h1>{{text}}</h1>{{datasetViewMain class="entryscape-details"}}'
    },
    {
      block: 'datasetFacetsHav',
      extends: 'facets_sv',
      exclude: 'license,type'
    },
    {
      block: 'datasetViewMain',
      extends: 'template',
      hl: '3',
      clickExpand: false,
      includeDataPreview: false,
      registry: false,
      class: 'esbView',
      htemplate: '<dl>' +
        '{{datasetVisualizations}}' +
        '<dt class="esbLabel">Utgivare</dt><dd>{{text relation="dcterms:publisher"}}</dd>' +
        '{{#ifprop "dcat:theme,dcterms:subject"}}' +
        '  <dt class="esbLabel">Kategori</dt><dd>{{#eachprop ' +
        '"dcat:theme"}}<span class="esbTag md5_{{md5}}">{{label}}</span>{{/eachprop}}' +
        '<span class="GEMET-category">' +
        '{{view rdformsid="hav:gemet" label="false" onecol="true"}}</span>' +
        '  </dd>' +
        '{{/ifprop}}' +
        '{{#ifprop "dcat:keyword"}}' +
        '  <dt class="esbLabel">Nyckelord</dt><dd>{{#eachprop "dcat:keyword" limit="20" ' +
        '  expandbutton="Visa mer" unexpandbutton="Visa mindre"}}<span class="esbTag">{{value}}</span>{{/eachprop}}</dd>' +
        '{{/ifprop}}' +
        '</dl>' +
        '{{#distributionList clickExpand="inherit" registry="inherit" ' +
        'includeDataPreview="inherit" hl="inherit"}}{{/distributionList}}' +
        '<h{{hl}}>Ytterligare information</h{{hl}}>\n' +
        '<div class="esbIndent">{{view filterpredicates="inherit"}}</div>\n',
    },
    {
      block: 'copyButton',
      extends: 'template',
      template: '<button role="button" class="btn btn-sm btn-primary esbCopyButton" title="kopiera url" onClick="navigator.clipboard.writeText(\'{{prop \'dcat:accessURL\'}}\').' +
          'then(() => {' +
          'this.classList.add(\'copied\');' +
          'setTimeout(()=>{this.classList.remove(\'copied\')}, 1000);' +
          '})"><i class="fas fa-clipboard" aria-hidden="true"></i></button>'
    },
    {
      block: 'distributionAccess',
      extends: 'template',
      template: '{{#ifprop "dcat:downloadURL"}}' +
          '{{#ifprop "dcat:downloadURL" min="2"}}Flera filer{{/ifprop}}' +
          '{{#ifprop "dcat:downloadURL" min="2" invert="true"}}' +
          '<a href="{{prop "dcat:downloadURL"}}" class="btn btn-sm btn-primary" style="white-space:nowrap;margin-left:10px" role="button" target="_blank">' +
          '<i class="fas fa-download" aria-hidden="true"></i>&nbsp;Ladda ned</a>' +
          '{{/ifprop}}' +
          '{{/ifprop}}' +
          '{{#ifprop "dcat:downloadURL" invert="true"}}' +
          '{{#ifprop "dcterms:format" literal="application/vnd.ogc.wms_xml,application/vnd.ogc.wfs_xml,application/vnd.ogc.wmts_xml"}}' +
          '{{copyButton}}' +
          '{{/ifprop}}' +
          '{{#ifprop "dcterms:format" literal="application/vnd.ogc.wms_xml,application/vnd.ogc.wfs_xml,application/vnd.ogc.wmts_xml" invert="true"}}' +
          '<a href="{{prop "dcat:accessURL"}}" class="btn btn-sm btn-primary" style="white-space:nowrap;margin-left:10px" role="button" target="_blank">' +
          '<i class="fas fa-external-link-square-alt" aria-hidden="true"></i>&nbsp;Åtkomst</a>' +
          '{{/ifprop}}' +
          '{{/ifprop}}',
    },
  ]
}]);
