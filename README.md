## This repo
This repo is available to HaV. The complete config for the sites (terms, external search and internal search) is made up of opendata-sv.js + special config for including HaV specific templates and blocks.

Included in this repo is:

- The HaV specific config files and stylesheets for upload in SiteVision
- Test pages for all pages
- Style sheet for all pages
- Information about how to set up the different pages including "HTML-snippets" for the SiteVision pages is given in index.html

## Other files needed to run the pages

The opendata-sv.js, dcat-ap2.json and the blocks library is published by HaV on their own servers at
[https://metadata.havochvatten.se/blocks/app.js](https://metadata.havochvatten.se/blocks/app.js), [https://metadata.havochvatten.se/blocks/opendata-sv.js](https://metadata.havochvatten.se/blocks/opendata-sv.js) and
[https://metadata.havochvatten.se/blocks/dcat-ap_se2.json](https://metadata.havochvatten.se/blocks/dcat-ap_se2.json).
New versions of the blocks library are provided for download as zip-file at [https://static.infra.entryscape.com/blocks/download/](https://static.infra.entryscape.com/blocks/download/)

New versions of opendata-extension, containing the opendata-sv.js and dcat-ap2.json files can be downloaded as zip-file at [https://static.infra.entryscape.com/blocks-ext/download/](https://static.infra.entryscape.com/blocks-ext/download/)
